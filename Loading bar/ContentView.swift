//
//  ContentView.swift
//  Loading bar
//
//  Created by Rime on 11/05/2023.
//

import SwiftUI

struct ContentView: View {
    @State var progress: CGFloat = 0
    var body: some View {
        VStack {
            ZStack(alignment:.leading){
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(width:300,height:10)
                
                Rectangle()
                    .foregroundColor(.blue)
                    .frame(width:progress*300,height:10)
                    .animation(.linear(duration:1.0))
            }
            .padding()
            
            Text("LOADING...")
        }
        .onAppear{
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {
                Timer in
                if progress < 1 {
                    progress += 1.0
                }
            }
        }
    }
    
}
