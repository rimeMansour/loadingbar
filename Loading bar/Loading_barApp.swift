//
//  Loading_barApp.swift
//  Loading bar
//
//  Created by Rime on 11/05/2023.
//

import SwiftUI

@main
struct Loading_barApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
